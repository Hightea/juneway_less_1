#!/bin/bash
CI_PROJECT_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects?search=Junless2.4" | jq '.[] | .id')
CI_PIPELINE_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines?per_page=1&page=1" | jq '.[] .id')
JOB_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs" | jq '.[]| select(.name == "deploy_prod") | .id')
curl -sk --request POST --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$JOB_ID/play"
