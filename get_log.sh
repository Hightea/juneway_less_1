JOB_ID=$(curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs" | jq '.[]| select(.name == "build_prod") | .id')
curl -sk --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/jobs/$JOB_ID/trace" | jq '.[]' > build_prod.log
